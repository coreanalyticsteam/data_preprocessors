"""
Format Gender data to uniform format for all entries

Assumptions:
1. Exact values are not of interest of us
2. If the categories change, the script key should be modified
3. Refer to function documentation while checking for consistency between data from different BU(or tables)

@author: Debajyoti
"""

import pandas as pd

loc = 'processed/LeadDetails_Gender.csv'

def gender(lead_id, gender, loc):
    """Digitise Gender data

    Key:
        Male: 1
        Female: 2
    
    Args:
        lead_id (pandas Series): primary key, used for indexing
        gender (pandas Series): Gender from the database
        loc (output file): typically csv, preferably in a subdirectory
        
    Returns:
        0 for failed cases
        1 for success
            Writes output to file
    """
    male = ['Male','M','male','m', '1']
    female = ['Female','F','female','f', '2']


    if lead_id.empty:
        print ("LeadID is empty")
        return 0
    else:
        lead_id = pd.Series(lead_id)
    
    if gender.empty:
        print ("Gender is empty")
        return 0
    else:
        gender = pd.Series(gender)

    gender = gender.astype(str)

    gender[gender.isin(male)] = 1
    gender[gender.isin(female)] = 2
    
    df = pd.concat([lead_id, gender], axis = 1, keys=['LeadID','Gender'])
    df.to_csv(loc)
    return 1