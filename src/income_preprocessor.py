"""
Classify AnnualIncome into Seven categories

assumption:
1. Exact values are not of interest of us
2. If the categories/slot division change, the script key should be modified


@author: Debajyoti
"""

import pandas as pd

loc = 'processed/LeadDetails_AnnualIncome.csv'

def income(lead_id, income, loc):
    """Separate AnnualIncome and assign levels

    Key:
        upto 3 lakhs: 1 
        3 - 5 lakhs: 2
        5 - 7 lakhs: 3
        7 - 10 lakhs: 4
        10 - 15 lakhs: 5
        15 lakhs and above: 6
        
    Args:
        lead_id (pandas Series): primary key, used for indexing
        income (pandas Series): AnnualIncome from the database
        loc (output file): typically csv, preferably in a subdirectory
        
    Returns:
        0 for failed cases
        1 for success
            Writes output to file
    """

#    df = pd.read_csv(locOld)
#    income = income.astype(str)

    if lead_id.empty:
        print ("LeadID is empty")
        return 0
    else:
        lead_id = pd.Series(lead_id)
    
    if income.empty:
        print ("AnnualIncome is empty")
        return 0
    else:
        income = pd.Series(income)

    #re-check value slots, taken from pb website(home)
    #TO DO: fix for user-values
    l1 = ['upto 3 lakhs','299999']
    l2 = ['3 - 5 lakhs','499999']
    l3 = ['5 - 7 lakhs','699999']
    l4 = ['7 - 10 lakhs','999999']
    l5 = ['10 - 15 lakhs','1499999']
    l6 = ['15 lakhs +']
    
    
    #TO DO: fix functional, strips space from input?
    '''
    Income = enum.Enum('Income',['upto 3 lakhs','3 - 5 lakhs','5 - 7 lakhs'
    ,'7 - 10 lakhs' , '10 - 15 lakhs', '15 lakhs +'])
    income = [[k.value for k in Income.x] for x in income]
    '''
    
    
    income[income.isin(l1)] = 1
    income[income.isin(l2)] = 2
    income[income.isin(l3)] = 3
    income[income.isin(l4)] = 4
    income[income.isin(l5)] = 5
    income[income.isin(l6)] = 6
    
    
    
    income = income.astype(float)
    df = pd.concat([lead_id, income], axis = 1, keys=['LeadID','IncomeLevel'])
    df.to_csv(loc)
    return 1