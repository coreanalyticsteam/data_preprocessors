"""
Plugin missing values for DOB from DateOfBirth, and calculate Age

Assumptions:
1. Only Year value is of interest to us
2. No values less than 1900 for year
3. All years are a four-digit value
4. Age is calculated based on system date

@author: Debajyoti
"""

import pandas as pd
from datetime import date


loc = 'processed/LeadDetails_DOB.csv'

def age(lead_id, dob,date_of_birth,loc):
    """Compute Birth Year and Age
    
    Args:
        lead_id (pandas Series): primary key, used for indexing
        dob (pandas Series): DOB from the database
        date_of_birth (pandas Series): DateOfBirth from database
        loc (output file): typically csv, preferably in a subdirectory
        
    Returns:
        0 for failed cases
        1 for success
            Writes output to file
    """

    if lead_id.empty:
        print ("LeadID is empty")
        return 0
    else:
        lead_id = pd.Series(lead_id)
    
    if dob.empty:
        print ("DOB is empty")
        return 0
    else:
        dob = pd.Series(dob)
        
    if date_of_birth.empty:
        print ("DateOfBirth is empty")
        return 0
    else:
        date_of_birth = pd.Series(date_of_birth)

    dob = dob.str.extract('(?P<year>(?:19|20)\d\d)') 
    dob.loc[dob.isnull()] = date_of_birth.str.extract('(?P<year>(?:19|20)\d\d)')


    dob = dob.astype(int)
    age = pd.Series(date.today().year - dob).astype(int)
    
    df = pd.concat([lead_id, dob, age], axis = 1, keys=['LeadID','BirthYear','Age'])
    df.to_csv(loc)
    return 1